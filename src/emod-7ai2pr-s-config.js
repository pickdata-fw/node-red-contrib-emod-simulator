/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
node-red-contrib-emod-simulator - The BSD 3-Clause License
**/

module.exports = function(RED) {
    function EMOD7AI2PRSCONFIG(config) {
        RED.nodes.createNode(this,config);
        
        this.sample_rate_period_ms = parseInt(config.sampleRatePeriodMs);
        this.pulse_width_ms = [
            parseInt(config.pulseWidthMsR1),
            parseInt(config.pulseWidthMsR2)
        ],
        this.mode = [
            config.modeI1,
            config.modeI2,
            config.modeI3,
            config.modeI4,
            config.modeI5,
            config.modeI6,
            config.modeI7
        ];
        this.event_mode = config.eventMode;
        this.interval_ms = parseInt(config.intervalMs);
        this.threshold = parseInt(config.threshold);
        this.low_limit = parseInt(config.lowLimit);
        this.high_limit = parseInt(config.highLimit);
    }
    RED.nodes.registerType("7AI+2PR-S-Config",EMOD7AI2PRSCONFIG);
}