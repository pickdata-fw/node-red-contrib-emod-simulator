/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
node-red-contrib-emod-simulator - The BSD 3-Clause License
**/

module.exports = function(RED) {
    function EMOD8SSRCONFIG(config) {
        RED.nodes.createNode(this,config);
        
        this.pulse_width_ms = [
            parseInt(config.pulseWidthMsR1),
            parseInt(config.pulseWidthMsR2),
            parseInt(config.pulseWidthMsR3),
            parseInt(config.pulseWidthMsR4),
            parseInt(config.pulseWidthMsR5),
            parseInt(config.pulseWidthMsR6),
            parseInt(config.pulseWidthMsR7),
            parseInt(config.pulseWidthMsR8)
        ];
    }
    RED.nodes.registerType("8SR-S-Config",EMOD8SSRCONFIG);
}