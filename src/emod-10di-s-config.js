/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
node-red-contrib-emod-simulator - The BSD 3-Clause License
**/

module.exports = function(RED) {
    function EMOD10DISCONFIG(config) {
        RED.nodes.createNode(this,config);

        this.pulse_filter_ms = [
            parseInt(config.pulseFilterMsI1),
            parseInt(config.pulseFilterMsI2),
            parseInt(config.pulseFilterMsI3),
            parseInt(config.pulseFilterMsI4),
            parseInt(config.pulseFilterMsI5),
            parseInt(config.pulseFilterMsI6),
            parseInt(config.pulseFilterMsI7),
            parseInt(config.pulseFilterMsI8),
            parseInt(config.pulseFilterMsI9),
            parseInt(config.pulseFilterMsI10)
        ];
        this.pulse_count_threshold = parseInt(config.threshold);
        this.event_mode = config.eventMode;
        //this.reset_pulse_count = Boolean(config.resetPulseCount); Disabled for now
    }
    RED.nodes.registerType("10DI-S-Config",EMOD10DISCONFIG);
}