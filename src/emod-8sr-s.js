/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
node-red-contrib-emod-simulator - The BSD 3-Clause License
**/

module.exports = function(RED) {
    const sr8_simulator = require('@pickdata/emod-js-simulator').SR8;

    function EMOD8SR_S(config) {
        RED.nodes.createNode(this,config);

        var node = this;

        node.configNode = RED.nodes.getNode(config.modConfig);
        node.modNumEnabled = config.modNumEnabled;
        node.modNum = parseInt(config.modNum);
        if (!node.modNumEnabled) {
            node.modNum = 1;
        }

        node.config_error = false;

        if (node.configNode == null) {
            node.status({fill:"red",shape:"ring",text:"Config error"});
            node.error("Node has no configuration");
            node.config_error = true;
            return;
        }

        var compareModConfig = function (node, other_node) {
            // Ambos nodos deberían tener la misma configuración, ya que corresponden al mismo módulo físico.
            if (!other_node.config_error && other_node.configNode.id != node.configNode.id) {
                throw new Error("Configuration mismatch between nodes "+node.id+" and "+other_node.id);
            }
        };

        try {
            RED.nodes.eachNode(function(n) {
                //Si node-red aún no ha instanciado el nodo n o está presente en un flow deshabilitado, esta función devuelve null y puede ser ignorado.
                var other_node = RED.nodes.getNode(n.id);
                if (other_node != null && other_node.id != node.id) {
                    if (other_node.type == node.type && other_node.modNum == node.modNum) {
                        // Esta función se llama con "other_node" para todos los nodos instanciados que sean del mismo tipo y número (mismo módulo físico) que "node"
                        compareModConfig(node, other_node);
                    }
                }
            });
        } catch (e) {
            node.error(e.message);
            node.status({fill:"red",shape:"ring",text:"Config error"});
            node.config_error = true;
            return;
        }

        node.simulator = sr8_simulator.getInstance(node.modNum);
        node.simulator.setPulseWitdth(node.configNode.pulse_width_ms);

        node.on('input', function(msg) {

            if ("set_one" in msg.payload && typeof msg.payload.set_one === 'object' && msg.payload.set_one !== null) {
                var relay_index = msg.payload.set_one.index;
                var relay_value = msg.payload.set_one.value;

                if (!Number.isInteger(relay_index) || relay_index < 0 || relay_index > 7) {
                    node.error("relay index must be in range.");
                    return;
                }

                if (typeof relay_value !== "boolean") {
                    node.error("relay value must be a boolean.");
                    return;
                }

                node.simulator.setOne(relay_index, relay_value);
                
                // Sends current status
                var relay_status = node.simulator.getOne(relay_index);
                node.send({payload: {status: {index: relay_index, value: relay_status}}});
            }

            else if ("set_all" in msg.payload && typeof msg.payload.set_all === "boolean") {
                node.simulator.setAll(msg.payload.set_all);

                // Sends current status
                var relay_status = node.simulator.getAll();
                node.send({payload: {all_status: relay_status}});
            }

            else if ("set" in msg.payload && Array.isArray(msg.payload.set)) {
                var relay_status = msg.payload.set;

                if (relay_status.length != 8) {
                    node.error("node input array length must be 8");
                    return;
                }

                node.simulator.set(relay_status);
    
                // Sends current status
                relay_status = node.simulator.getAll();
                node.send({payload: {all_status: relay_status}});
            }

            else if ("get_all" in msg.payload) {         
                var relay_status = node.simulator.getAll();

                node.send({payload: {all_status: relay_status}});
            }

            else if ("get_one" in msg.payload && Number.isInteger(msg.payload.get_one)) {
                var relay_index = msg.payload.get_one;

                if (relay_index < 0 || relay_index > 7) {
                    node.error("relay index must be in range.");
                    return;
                }

                var relay_status = node.simulator.getOne(relay_index);

                node.send({payload: {status: {index: relay_index, value: relay_status}}});
            }

            else {
                node.error("unknown method call.");
            }
        });

        node.status({});
    }
    RED.nodes.registerType("8SR-S",EMOD8SR_S);
}
