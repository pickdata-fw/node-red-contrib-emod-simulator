# node-red-contrib-emod-simulator
[![Platform](https://img.shields.io/badge/platform-Node--RED-red)](https://nodered.org)

[Node-RED][1] package to easy integrate [eManager][2] expansion modules to monitor and control digital & analogue inputs, relays, energy meters and more. eManager is an OEM industrial controller that has been built with [eMOD, the industrial modular hardware technology][3].

The eManager Platform is an open source embedded platform, built on Linux and designed for easy develop industrial and edge computing applications. It’s ready to run, build, and IoT connect out-of-the-box.

# Installation

You can install the nodes using node-red's "Manage palette" in the side bar.

Or run the following command in the root directory of your Node-RED installation

    npm install node-red-contrib-emod-simulator --save


# Usage

## 8SR (8 Signal Relays)

![8SR](https://bitbucket.org/pickdata-fw/node-red-contrib-emod-simulator/raw/512262c3ef46ce01fad257510fb12fd5970b9e88/src/images/8SR-node.png "8SR")

Connects to a [8SR eMOD module][4] to manage the 8 relays individually or jointly.

It allows to manage the following functionalities on the module:

- Set the pulse width for each relay
- Activate or deactivate one, some or all relays
- Get the current relays status

### Available functions:
- **get_one**: obtains the status of a single relay.

  Examples:

  To obtain the status of the relay 0:

  ```json
  {"get_one": 0}
  ```

  In this case, the response tells us that relay 0 is deactivated:

  ```json
  {"status": {"index":0, "value":false}}
  ```

- **get_all**: obtains the status of all relays.

  Examples:

  To obtain the status of all relays:

  ```json
  {"get_all": "status"}
  ```

  In this case, the first and fourth relays are activated:

  ```json
  {"all_status": [true, false, false, true, false, false, false, false] }
  ```

- **set_one**: activates or deactivates a single relay. On completion of action, input message with result is sent to output port.

  Examples:

  Activates relay 0:

  ```json
  {"set_one": {"index": 0, "value": true}}
  ```

  Output tells us that relay 0 is activated:

  ```json
  {"status": {"index":0, "value":true}}
  ```

  Deactivates relay 0:

  ```json
  {"set_one": {"index": 0, "value": false}}
  ```

  Output tells us that relay 0 is deactivated:

  ```json
  {"status": {"index":0, "value":false}}
  ```

- **set**: activates or deactivates some relays. On completion of action, input message with result is sent to output port.

  Parameters:

  An 8-element array of one among the following:
  - true: activates relay
  - false: deactivates relay
  - null: does not change the status of the relay

  Examples:

  Activate the first and fourth relays and deactivate the eighth:

  ```json
  {"set": [true, null, null, true, null, null, null, false]}
  ```

  Output tells us all relay status:

  ```json
  {"all_status":[true,true,true,true,false,true,false,false]}
  ```

- **set_all**: activates or deactivates all relays. On completion of action, input message with result is sent to output port.

  Examples:

  To activate all relays:

  ```json
  {"set_all": true}
  ```

  Output tells us all relay status:

  ```json
  {"all_status":[true,true,true,true,true,true,true,true]}
  ```

  To deactivate all relays:

  ```json
  {"set_all": false}
  ```

  Output tells us all relay status:

  ```json
  {"all_status":[false,false,false,false,false,false,false,false]}
  ```

### Flow example

The following flow shows an example of different API calls:

![8SR Demo](https://bitbucket.org/pickdata-fw/node-red-contrib-emod-simulator/raw/512262c3ef46ce01fad257510fb12fd5970b9e88/src/images/8SR-demo.png "8SR Demo")

&nbsp;

## 10DI (10 Digital Inputs)

![10DI](https://bitbucket.org/pickdata-fw/node-red-contrib-emod-simulator/raw/512262c3ef46ce01fad257510fb12fd5970b9e88/src/images/10DI-node.png "10DI")

Connects to a [10DI eMOD module][5] to manage the 10 digital inputs individually or jointly.

It allows to manage the following functionalities on the module:

- Configure module parameters
- Poll input status for one or all inputs
- Poll pulse counter for one or all inputs
- Reset pulse counters

### Working modes:

10DI eMOD module can operate in two different ways: **Poll mode** and **Event mode**.

In **Poll mode**, user starts the flow and asks for data when he needs it.

In **Event mode** user configures certain condition when he wants to be notified and it is the module that notifies him when it has been met.

Module can operate in the following working modes:
- Poll
- [Event] Input value change: module notifies when a digital input changes
- [Event] Pulse count change: module notifies when the pulse counter surpases the threshold from last notification

### Available functions

- **get_one** (only in Poll mode): obtains the status or pulse count of a single digital input.

  Examples:

  To obtain the status of a single digital input:

  ```json
  {"get_one": {"index": 3, "mode": "status"}}
  ```

  In this case, fourth digital input is in high state:

  ```json
  {"status": {"index": 3, "value": true}}
  ```

  To obtain the pulse count of a digital input:

  ```json
  {"get_one": {"index": 3, "mode": "pulse_count"}}
  ```

  In this case, pulse count of fourth digital input is 15:

  ```json
  {"counter": {"index": 3, "value": 15}}
  ```

- **get_all** (only in Poll mode): obtains the status or pulse count of all digital inputs.

  Examples:

  To obtain the status of all digital input:

  ```json
  {"get_all": "status"}
  ```

  In this case, only fourth digital input is high state:

  ```json
  {"all_status": [false, false, false, true, false, false, false, false, false, false] }
  ```

  To obtain the pulse count of all digital input:

  ```json
  {"get_all": "pulse_count"}
  ```

  Function returns following pulse counts:

  ```json
  {"all_counters": [0, 5, 10, 2, 23, 5, 71, 3, 0, 0] }
  ```

- **reset_pulse_count**: resets one or all inputs.

  Examples:

  Resets the ninth input:

  ```json
  {"reset_pulse_count": 8}
  ```

  Resets all input:

  ```json
  {"reset_pulse_count": "all"}
  ```

### Flow example

In the following flow shows an example of different API calls:

![10DI Demo](https://bitbucket.org/pickdata-fw/node-red-contrib-emod-simulator/raw/512262c3ef46ce01fad257510fb12fd5970b9e88/src/images/10DI-demo.png "10DI Demo")

&nbsp;

## 7AI+2PR (7 Analog Inputs + 2 Power Relays)

![7AI2PR](https://bitbucket.org/pickdata-fw/node-red-contrib-emod-simulator/raw/512262c3ef46ce01fad257510fb12fd5970b9e88/src/images/7AI2PR-node.png "7AI2PR")

Connects to a [7AI+2PR eMOD module][6] to manage the 7 analog inputs and 2 relays individually or jointly.

It allows to manage the following functionalities on the module:

- Set the pulse width for each relay
- Set the input mode (current or voltage) for each analog input and also the working mode
- Get the input value for one or all inputs
- Activate or deactivate one, some or all relays
- Get the current relays status

### Working modes:

7AI can operate in two different ways: **Poll mode** and **Event mode**

In **Poll mode**, user starts the flow and asks for data when he needs it.

In **Event mode** user configures certain condition when he wants to be notified and it is the module that notifies him when it has been met.

Module can operate in the following working modes:
- Poll
- [Event] Time interval: module keeps notifying values of all inputs in a fixed time interval
- [Event] Value threshold: module notifies when the measured value in any input surpases the threshold from last notification
- [Event] Value within range: module notifies when the measured value in any input enters within a value range
- [Event] Value out of range: module notifies when the measured value in any input falls out of a value range

2PR only can operate in **Poll mode**

This module has 2 outputs: one for the relays and the other for the analog inputs.

### Available functions for analog inputs

- **ai_get_one** (only in Poll mode): obtains the value of a single analog input.

  Examples:

  To obtain the value of a single analog input:

  ```json
  {"ai_get_one": 3}
  ```

  In this case, fourth analog input is configured as voltage and its value is 3.1 V:

  ```json
  {"voltage": {"index": 3, "value": 3.1}}
  ```

- **ai_get_all** (only in Poll mode): obtains the value of all analog inputs.

  Examples:

  To obtain the value of all analog inputs:

  ```json
  {"ai_get_all": "status"}
  ```

  In this case, the first three inputs are configured as voltage and the rest as current. Each one has a different value:

  ```json
  {"values": [{"voltage": {"index": 0, "value": 5.2}}, {"voltage": {"index": 1, "value": 3.2}} , {"voltage": {"index": 2, "value": 4.8}}, {"current": {"index": 3, "value": 0.3}}, {"current": {"index": 4, "value": 0.2}}, {"current": {"index": 5, "value": 0.4}}, {"current": {"index": 6, "value": 0.3}} ]}
  ```

### Available functions for relays

- **relay_get_one**: obtains the status of a single relay.

  Examples:

  To obtain the status of the relay 0:

  ```json
  {"relay_get_one": 0}
  ```

  In this case, the response tells us that relay 0 is deactivated:

  ```json
  {"status": {"index":0, "value":false}}
  ```

- **relay_get_all**: obtains the status of all relays.

  Examples:

  To obtain the status of all relays:

  ```json
  {"relay_get_all": "status"}
  ```

  In this case, only the first relay is activated:

  ```json
  {"all_status": [true, false] }
  ```

- **relay_set_one**: activates or deactivates a single relay. On completion of action, input message with result is sent to output port.

  Examples:

  Activates relay 0:

  ```json
  {"relay_set_one": {"index": 0, "value": true}}
  ```

  Output tells us that relay 0 is activated:

  ```json
  {"status": {"index":0, "value":true}}
  ```

  Deactivates relay 0:

  ```json
  {"relay_set_one": {"index": 0, "value": false}}
  ```

  Output tells us that relay 0 is deactivated:

  ```json
  {"status": {"index":0, "value":false}}
  ```

- **relay_set**: activates or deactivates some relays. On completion of action, input message with result is sent to output port.

  Parameters:

  An 8-element array of one among the following:
  - true: activates relay
  - false: deactivates relay
  - null: does not change the status of the relay

  Examples:

  Activate the first and deactivate the second:

  ```json
  {"relay_set": [true, false]}
  ```

  Output tells us all relay status:

  ```json
  {"all_status":[true,false]}
  ```

- **relay_set_all**: activates or deactivates all relays. On completion of action, input message with result is sent to output port.

  Examples:

  To activate all relays:

  ```json
  {"relay_set_all": true}
  ```

  Output tells us all relay status:

  ```json
  {"all_status":[true,true]}
  ```

  To deactivate all relays:

  ```json
  {"relay_set_all": false}
  ```

  Output tells us all relay status:

  ```json
  {"all_status":[false,false]}
  ```

### Flow example

In the following flow shows an example of different API calls:

![7AI2PR Demo](https://bitbucket.org/pickdata-fw/node-red-contrib-emod-simulator/raw/512262c3ef46ce01fad257510fb12fd5970b9e88/src/images/7AI2PR-demo.png "7AI2PR Demo")


[1]:https://nodered.org
[2]:https://www.pickdata.net/emanager-iiot-modular-controller
[3]:https://www.pickdata.net/emod-oem-iot-modular-device
[4]:https://www.pickdata.net/sites/default/files/Datasheet_8SignalRelay_EN.pdf
[5]:https://www.pickdata.net/sites/default/files/Datasheet_10DigitalInput_EN.pdf
[6]:https://www.pickdata.net/sites/default/files/Datasheet_7AnalogueInput_2PowerRelay_EN.pdf
